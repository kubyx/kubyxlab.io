import React from 'react';
import styled from '@emotion/styled';

export const Lead = styled('p')`
  font-size: 1.4rem;
  font-weight: 300;
`;

export default Lead;
