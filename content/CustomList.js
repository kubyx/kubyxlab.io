import * as React from 'react';

export default function CustomList({ children }) {
  return (
    <ul class="custom-list" style={{ listStyle: `none`, padding: 0, margin: 0 }}>
      {children}
    </ul>
  )
}
