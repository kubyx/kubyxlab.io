import * as React from 'react';

export default function CustomListItem({ bullet, children }) {
  return (
    <li class="custom-list-item" style={{ listStyle: `none`, paddingLeft: `1rem` }}>
      <span class="custom-list-item-bullet" style={{ paddingRight: `0.5rem` }}>{ bullet }</span>
      {children}
    </li>
  )
}
