---
title: "Glossary"
metaTitle: "Kubyx glossary of terminology"
metaDescription: "Useful words to know when describing entities and relations within Kubyx"
---

- A **workflow** describes how a case will be handled when it happens.  It consists of nodes and links between them, always starts at the `Start` node and finishes at an `End` node.
- A **node** describes how a task will be done, and passes the flow on to the next connected node when the task is complete.
- A **case** is created in a workflow when work needs to be done. As it flows through each node tasks are generated.
- A **task** is a single unit of work, assigned to an agent pool, describing data inputs and outputs and validation.
- **Scripted agents** accept and perform tasks via API libraries.
- **Human agents** accept and perform tasks via an intuitive web UI.
- **Agent pools** are teams of either human or scripted agents capable of completing certain types of tasks (e.g. designers, developers)
