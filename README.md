# kubyx-docs

This is the documentation repository for the Kubyx open source project.

It is based on Gatsby and the Gitbook starter documentation project.

- Write using Markdown / [MDX](https://github.com/mdx-js/mdx)
- GitBook style theme
- Syntax Highlighting using Prism [`Bonus`: Code diff highlighting]
- Search Integration with Algolia
- Progressive Web App, Works Offline
- Automatically generated sidebar navigation, table of contents, previous/next
- Dark Mode toggle
- Rich embeds and live code editor using MDX

## 🚀 Quickstart

Get started by running the following commands:

```
$ git clone <repo-url>
$ cd <project-name>
$ npm install
$ npm start
```

Visit `http://localhost:8000/` to view the app.

See the gatsby-gitbook-starter project for more documentation.
